const { checkLength, checkAlphabet, checkDigit, checkSymbol, checkPassword } = require('./password')
describe('Test Password Length', () => {
  test('should 8 characters to be true', () => {
    expect(checkLength('12345678')).toBe(true)
  })
  test('should 7 characters to be false', () => {
    expect(checkLength('1234567')).toBe(false)
  })
  test('should 25 characters to be true', () => {
    expect(checkLength('1111111111111111111111111')).toBe(true)
  })

  test('should 26 characters to be false', () => {
    expect(checkLength('11111111111111111111111111')).toBe(false)
  })
})
describe('Test Alphabet', () => {
  test('should has alphabet abcdefghijklmnopqrstuvwxyz in password', () => {
    expect(checkAlphabet('abcdefghijklmnopqrstuvwxyz')).toBe(true)
  })
  test('should has alphabet A in password', () => {
    expect(checkAlphabet('AZ')).toBe(true)
  })
  test('should has not alphabet in password', () => {
    expect(checkAlphabet('15634')).toBe(false)
  })
  test('should has alphabet A1 in password', () => {
    expect(checkAlphabet('A1')).toBe(true)
  })
})
describe('Test Digit', () => {
  test('should has digit in password', () => {
    expect(checkDigit('123')).toBe(true)
  })
})
describe('Test Symbol', () => {
  test('should have Symbol ! in password to be true', () => {
    expect(checkSymbol('11!11')).toBe(true)
  })
})
describe('Test password', () => {
  test('should password !Stang0918747783 to be true ', () => {
    expect(checkPassword('!Stang0918747783')).toBe(true)
  })
})
