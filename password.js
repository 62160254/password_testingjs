const checkLength = function (password) {
  return password.length >= 8 && password.length <= 25
}
const checkAlphabet = function (password) {
  const alphabet = 'abcdefghijklmnopqrstuvwxyz'
  for (const ch of password) {
    if (alphabet.includes(ch.toLowerCase())) return true
  }
  return false
  // return /[a-zA-Z]/.test(password)
}
const checkDigit = function (password) {
  const digit = '0123456789'
  for (const nu of password) {
    if (digit.includes(nu)) return true
  }
  return false
  // return /[0-9]/.test(password)
}
const checkSymbol = function (password) {
  const symbol = '!"#$%&()*+,-./:;<=>?@[]^_`{|}~'
  for (const ch of password) {
    if (symbol.includes(ch.toLowerCase())) return true
  }
  return false
}
const checkPassword = function (password) {
  return checkLength(password) &&
  checkAlphabet(password) &&
  checkDigit(password) &&
  checkSymbol(password)
}
module.exports = {
  checkLength,
  checkAlphabet,
  checkDigit,
  checkSymbol,
  checkPassword
}
